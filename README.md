# Command line Interface for training and testing on the CIFAR-100 dataset

## Dataset
https://www.cs.toronto.edu/~kriz/cifar.html

This program has functionality to reduce the amount of data in each sub class.

## Training
change to project directory

`~$ cd archangel-test/task-2`
##### Quickstart
`~$ python train_cifar100.py --save-dir /home/saul/cifar100/test`
##### Default values

batch size = 32

epochs = 100

image dims = (32, 32, 3)

learning rate = 1e-3

number in class = 100

##### Example
`~$ python train_cifar100.py --save-dir /home/saul/cifar100/test --epochs 500 --batch-size 128 number-in-class 200`

## Testing

`~$ python test_cifar100.py --model-dir /home/saul/cifar100/moresteps --labels-path meta
`
model-dir is the director of the model you would like to test

labels-path is the meta data file containing class information, this is contained in the repository

The testing program saves two confusion matrices, one for the main classes and one for the subclasses.
### Tensorboard

There is also a tensorboard event file which contains accuaracy and loss information per epoch.

This can be accessed by:

`tensorboard --logdir model-directory`