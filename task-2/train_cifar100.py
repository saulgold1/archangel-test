import keras
from keras.models import Sequential
from keras.layers import Dense, Activation, Flatten, Dropout, BatchNormalization
from keras.layers import Conv2D, MaxPooling2D
from keras.datasets import cifar100
from keras.layers.core import Activation
from keras.layers.core import Flatten
from keras.layers.core import Dropout
from keras.layers.core import Dense
from keras import backend as K
from keras.preprocessing.image import ImageDataGenerator
from keras.optimizers import Adam
import matplotlib.pyplot as plt
import numpy as np
import argparse
import os
import sys

class SmallerVGGNet:
    @staticmethod
    def build(width, height, depth, classes, finalAct="softmax"):
        # initialize the model along with the input shape to be
        # "channels last" and the channels dimension itself
        model = Sequential()
        inputShape = (height, width, depth)
        chanDim = -1

        # if we are using "channels first", update the input shape
        # and channels dimension
        if K.image_data_format() == "channels_first":
            inputShape = (depth, height, width)
            chanDim = 1

        # CONV => RELU => POOL
        model.add(Conv2D(32, (3, 3), padding="same",
                         input_shape=inputShape))
        model.add(Activation("relu"))
        model.add(BatchNormalization(axis=chanDim))
        model.add(MaxPooling2D(pool_size=(3, 3)))
        model.add(Dropout(0.25))

        # (CONV => RELU) * 2 => POOL
        model.add(Conv2D(64, (3, 3), padding="same"))
        model.add(Activation("relu"))
        model.add(BatchNormalization(axis=chanDim))
        model.add(Conv2D(64, (3, 3), padding="same"))
        model.add(Activation("relu"))
        model.add(BatchNormalization(axis=chanDim))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(0.25))

        # (CONV => RELU) * 2 => POOL
        model.add(Conv2D(128, (3, 3), padding="same"))
        model.add(Activation("relu"))
        model.add(BatchNormalization(axis=chanDim))
        model.add(Conv2D(128, (3, 3), padding="same"))
        model.add(Activation("relu"))
        model.add(BatchNormalization(axis=chanDim))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(0.25))

        # first (and only) set of FC => RELU layers
        model.add(Flatten())
        model.add(Dense(1024))
        model.add(Activation("relu"))
        model.add(BatchNormalization())
        model.add(Dropout(0.5))

        # use a *softmax* activation for single-label classification
        # and *sigmoid* activation for multi-label classification
        model.add(Dense(classes))
        model.add(Activation(finalAct))

        # return the constructed network architecture
        return model

def get_class_sample(class_amount, x_train,y_train, y_train_c):
    #order the image and label data by class
    x_train = x_train[np.argsort(y_train,axis=0)]
    y_train = np.sort(y_train, axis=0)
    y_train_c = y_train_c[np.argsort(y_train,axis=0)]


    #remove unecesary dimension
    x_train = np.squeeze(x_train, axis=1)
    y_train_c = np.squeeze(y_train_c, axis=1)

    #initialise new data variables
    new_x_train = None
    new_y_train = None
    new_y_train_c = None

    #A loop of size 100 because there are 100 classes
    for i in range(0,100):
        print i
        #select the first 100
        x_class = x_train[np.where(y_train==i)[0]][0:class_amount]
        y_class = y_train[np.where(y_train==i)[0]][0:class_amount]
        y_class_c = y_train_c[np.where(y_train==i)[0]][0:class_amount]

        if new_x_train is None:
            new_x_train = x_class
            new_y_train = y_class
            new_y_train_c = y_class_c
        else:
            new_x_train = np.concatenate((new_x_train,x_class), axis = 0)
            new_y_train = np.concatenate((new_y_train,y_class),axis=0)
            new_y_train_c = np.concatenate((new_y_train_c,y_class_c),axis=0)


    return new_x_train, new_y_train, new_y_train_c

def unpickle(file):
    import cPickle
    with open(file, 'rb') as fo:
        dict = cPickle.load(fo)
    return dict

def parse_args(args):
    parser     = argparse.ArgumentParser(description='Simple training script for training CIFAR 100.')

    parser.add_argument('--save-dir', help='Directory to save the model and results',type = str, dest = 'save_dir')
    parser.add_argument('--epochs', help='number of epochs to run for, default is 100',default=100,type = int, dest = 'epochs')
    parser.add_argument('--batch-size', help='batch size', default = 32,type = int, dest = 'batch_size')
    parser.add_argument('--learning-rate', help='initialised learning rate', default=1e-3,type = float, dest = 'learning_rate')

    parser.add_argument('--image-dims', help='dimension of images', default=(32, 32, 3), dest = 'image_dims')
    parser.add_argument('--number-in-class', help = ' how many training images to use in each class',default = 100,type=int, dest = 'num_class')

    return parser.parse_args(args)


def main(args = None):
    # parse arguments
    if args is None:
        args = sys.argv[1:]
    args = parse_args(args)

    log_dir = args.save_dir

    #create the log dir if it dosen't already exist
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)

    model_save_path = os.path.join(log_dir, 'model.h5')
    save_path = os.path.join(log_dir,'100perclassgraph.jpg')
    num_classes = args.num_class
    image_dims = args.image_dims
    epochs = args.epochs
    learning_rate = args.learning_rate
    batch_size = args.batch_size

    #load the cifar 100 dataset with coarse and fine labels
    (x_train, y_train_f), (x_test, y_test_f) = cifar100.load_data(label_mode='fine')
    (x_train, y_train_c), (x_test, y_test_c) = cifar100.load_data(label_mode='coarse')

    # reduce the data size to only 100 images per class
    x_train, y_train_f, y_train_c = get_class_sample(num_classes, x_train, y_train_f, y_train_c)

    # convert the labels to binary categories
    y_train_f = keras.utils.to_categorical(y_train_f, 100)
    y_test_f = keras.utils.to_categorical(y_test_f, 100)
    y_train_c = keras.utils.to_categorical(y_train_c, 20)
    y_test_c = keras.utils.to_categorical(y_test_c, 20)

    # combine the coarse and fine labels
    y_test = np.concatenate((y_test_c, y_test_f), axis=1).astype(np.uint8)
    y_train = np.concatenate((y_train_c, y_train_f), axis=1).astype(np.uint8)

    # construct the image generator for data augmentation
    aug = ImageDataGenerator(rotation_range=25, width_shift_range=0.1,
                             height_shift_range=0.1, shear_range=0.2, zoom_range=0.2,
                             horizontal_flip=True, fill_mode="nearest")

    # initialize the model using a sigmoid activation as the final layer
    # in the network so we can perform multi-label classification
    print("[INFO] compiling model...")

    model = SmallerVGGNet.build(
        width=image_dims[1], height=image_dims[0],
        depth=image_dims[2], classes=120,
        finalAct="sigmoid")

    # initialize the optimizer
    opt = Adam(lr=learning_rate, decay=learning_rate / epochs)

    # compile the model using binary cross-entropy
    model.compile(loss="binary_crossentropy", optimizer=opt,
                  metrics=["accuracy"])

    # Initialise a tensorboard call_back to save the results every epoch
    callbacks = []

    tensorboard_callback = keras.callbacks.TensorBoard(
        log_dir=log_dir,
        histogram_freq=0,
        batch_size=batch_size,
        write_graph=True,
        write_grads=False,
        write_images=False,
        embeddings_freq=0,
        embeddings_layer_names=None,
        embeddings_metadata=None
    )
    callbacks.append(tensorboard_callback)

    # train the network
    print("[INFO] training network...")
    H = model.fit_generator(
        aug.flow(x_train, y_train, batch_size=batch_size),
        validation_data=(x_test, y_test),
        steps_per_epoch=len(x_test) // batch_size,
        epochs=epochs, verbose=1, callbacks=callbacks)

    # save the model to disk
    print("[INFO] serializing network...")
    model.save(model_save_path)
    print(H)
    # plot the training loss and accuracy
    # plt.style.use("ggplot")
    # plt.figure()
    # N = epochs
    # plt.plot(np.arange(0, N), H.history["loss"], label="train_loss")
    # plt.plot(np.arange(0, N), H.history["val_loss"], label="val_loss")
    # plt.plot(np.arange(0, N), H.history["acc"], label="train_acc")
    # plt.plot(np.arange(0, N), H.history["val_acc"], label="val_acc")
    # plt.title("Training Loss and Accuracy")
    # plt.xlabel("Epoch #")
    # plt.ylabel("Loss/Accuracy")
    # plt.legend(loc="upper left")
    # plt.savefig(save_path)

if __name__ =='__main__':
    ##For debugging
    # a = [ '--save-dir', '/home/saul/Documents/cifar100/100perclass/softmax/', '--epochs','10']
    # main(a)

    ##For running
    main()
