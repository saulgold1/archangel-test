import keras
import numpy as np
from keras.datasets import cifar100
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
from sklearn.utils.multiclass import unique_labels
import os
import sys
import argparse
import csv


def predict(img, label, model, classes):

    predictions = model.predict(np.expand_dims(img, axis = 0))
    predictions = predictions[0]
    assert(len(predictions)==len(classes))

    class_idx = np.argsort(predictions[:20])[-1]
    sub_idx = np.argsort(predictions[20:])[-1]

    label_class = classes[:20][class_idx]
    label_subclass = classes[20:][sub_idx]

    real_class, real_subclass = label2class(label,classes)

    return label_class,real_class, label_subclass, real_subclass

def unpickle(file):
    import cPickle
    with open(file, 'rb') as fo:
        dict = cPickle.load(fo)
    return dict


def label2class(label, classes):
    # get index of class
    idx_class = np.where(label[:20]==1)
    # get index of subclass
    idx_subclass = np.where(label[20:]==1)
    # get class name
    label_class = classes[:20][idx_class]
    # get sub-class name
    label_subclass = classes[20:][idx_subclass]

    return label_class, label_subclass

#from https://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html#sphx-glr-auto-examples-model-selection-plot-confusion-matrix-py
def plot_confusion_matrix(cm, classes, add_text = True,
                          normalize=False,
                          title=None,
                          cmap=plt.cm.Blues):

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    fig, ax = plt.subplots()
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    ax.figure.colorbar(im, ax=ax)
    # We want to show all ticks...
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           # ... and label them with the respective list entries
           xticklabels=classes, yticklabels=classes,
           title=title,
           ylabel='True label',
           xlabel='Predicted label')

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    if add_text is True:
        fmt = '.2f' if normalize else 'd'
        thresh = cm.max() / 2.
        for i in range(cm.shape[0]):
            for j in range(cm.shape[1]):
                ax.text(j, i, format(cm[i, j], fmt),
                        ha="center", va="center",
                        color="white" if cm[i, j] > thresh else "black")
    fig.tight_layout()
    return ax

def parse_args(args):
    parser     = argparse.ArgumentParser(description='Simple training script for training CIFAR 100.')

    parser.add_argument('--model-dir', help='Directory to save the model and results',type = str, dest = 'model_dir')
    parser.add_argument('--labels-path', help='path of the labels meta data',type = str, dest = 'labels_path')

    return parser.parse_args(args)

def main(args = None):
    # parse arguments
    if args is None:
        args = sys.argv[1:]
    args = parse_args(args)


    model_dir = args.model_dir
    labels_path = args.labels_path

    results_dir = os.path.join(model_dir, 'results')

    if not os.path.exists(results_dir):
        os.makedirs(results_dir)
    model_path = os.path.join(model_dir, 'model.h5')
    label_bin_path = os.path.join(model_dir, 'label.pkl')
    csv_path = os.path.join(results_dir, 'results.csv')


    model = keras.models.load_model(model_path)

    (x_train, y_train_f), (x_test, y_test_f) = cifar100.load_data(label_mode='fine')
    (x_train, y_train_c), (x_test, y_test_c) = cifar100.load_data(label_mode='coarse')

    y_train_f = keras.utils.to_categorical(y_train_f, 100)
    y_test_f = keras.utils.to_categorical(y_test_f, 100)

    y_train_c = keras.utils.to_categorical(y_train_c, 20)
    y_test_c = keras.utils.to_categorical(y_test_c, 20)

    # combine the coarse and fine labels
    y_train = np.concatenate((y_train_c, y_train_f), axis=1).astype(np.uint8)
    y_test = np.concatenate((y_test_c, y_test_f), axis=1).astype(np.uint8)

    loss, acc = model.evaluate(x_test, y_test)

    labels = unpickle(labels_path)
    classes = labels[labels.keys()[1]] + labels[labels.keys()[0]]
    class_true = []
    class_pred = []
    sub_true = []
    sub_pred = []
    for i, label in enumerate(y_test):
        label_class, real_class, label_subclass, real_subclass = predict(x_test[i], label, model, np.array(classes))
        class_true.append(real_class)
        class_pred.append(label_class)
        sub_true.append(real_subclass)
        sub_pred.append(label_subclass)

        print(str(i) + '/' + str(len(y_test)))

    # write results to csv
    all_results = []
    all_results.append(['predicted class'] +class_pred)
    all_results.append(['actual class'] +class_true)
    all_results.append(['predicted subclass'] + sub_pred)
    all_results.append(['actual subclass'] +sub_true)

    with open(csv_path, "wb") as f:
        writer = csv.writer(f)
        writer.writerows(all_results)

    #make confusion matrix of results
    class_confusion = confusion_matrix(class_true, class_pred, labels=classes[:20])
    sub_confusion = confusion_matrix(sub_true, sub_pred, labels=classes[20:])

    #plot and save confustion matrix
    #depending on matplotlib backend, the saved figs may be too small
    # # Option 1
    # # QT backend
    # manager = plt.get_current_fig_manager()
    # manager.window.showMaximized()
    #
    # # Option 2
    # # TkAgg backend
    # manager = plt.get_current_fig_manager()
    # manager.resize(*manager.window.maxsize())
    #
    # # Option 3
    # # WX backend
    # manager = plt.get_current_fig_manager()
    # manager.frame.Maximize(True)

    plot_confusion_matrix(class_confusion, classes[:20], title='Class Confusion Matrix')
    plt.savefig(os.path.join(results_dir, 'class_confusion.jpg'))
    plot_confusion_matrix(sub_confusion, classes[20:], add_text=False, title='Sub-Class Confusion Matrix')
    plt.savefig(os.path.join(results_dir, 'sub-class_confusion.jpg'))

    # plt.show()

if __name__ == '__main__':
    ##for debugging
    # a = ['--model-dir','/home/saul/cifar100/moresteps',
    #    '--labels-path','meta']
    # main(a)

    ##for running
    main()
